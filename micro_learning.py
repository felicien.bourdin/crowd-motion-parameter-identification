import numpy as np
import matplotlib.pyplot as plt
import torch
import torch.optim as optim

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(device)

def positive_penalizer(x):
    return 100*torch.minimum(x,torch.tensor(0.))**2


def bc_diff_pytorch(D):
    return torch.remainder(D - lim, 2 * lim) - lim

def distmat_pytorch(X,Y):
  return torch.sqrt(torch.sum(bc_diff_pytorch(X[:,None,:] - Y[None,:,:])**2+0.0001, dim=2 ))

def phi_pytorch(r, AR, AC, lR, lC):
    return AR / lR ** 2 * torch.exp(-r / lR ** 2) - AC / lC ** 2 * torch.exp(-r / lC ** 2)

def Speed_pytorch(X, AR, AC, lR, lC):
    return 4 / n * torch.sum(phi_pytorch(distmat_pytorch(X, X), AR, AC, lR, lC)[:, :, None] * bc_diff_pytorch(
        X[:, None, :] - X[None, :, :]), dim=1)

def kernel(X, Y):
    # print(distmat_pytorch(X,Y))
    # breakpoint()
    return - distmat_pytorch(X, Y)

def MMD(X, Y):
    n = X.shape[0]
    m = Y.shape[0]
    a = torch.sum(kernel(X, X)) / n ** 2 + \
        torch.sum(kernel(Y, Y)) / m ** 2 - \
        2 * torch.sum(kernel(X, Y)) / (n * m)
    return a

def run(X, tau, niter, AR, AC, lR, lC):
    Z = X
    Zsvg = Z.unsqueeze(0)
    for it in range(niter):
        g = Speed_pytorch(Z, AR, AC, lR, lC)
        Z = bc_diff_pytorch(Z + tau * g)
        Zsvg = torch.cat((Zsvg, Z.unsqueeze(0)), dim=0)
    return Z, Zsvg

## --------------------------- PARAMETERS -------------------------

## Simulation parameters
n = 50  # number of points
d = 2  # dimension
lim = 1  # grid size
niter = 50
tau = torch.tensor(.01)  # time step
AR, AC, lR, lC = [4, 1, 0.2, 2]
AR, AC, lR, lC = torch.tensor([AR, AC, lR, lC], device=device)


## Learning parameters
batchsize = 5 # Number of sets of initial positions
epoch = 500
lr = .2 # Learning rate
ntest_al = 1 # Number of gradient descents
loss = 100000 # Initial Loss = \infty

## --------------------------- GENERATE ZOBS -------------------------


## Initial positions
Xinit = torch.stack([torch.tensor(2 * lim * (torch.rand(size=(n, 2)) - 0.5)) for x in range(batchsize)]).to(device)
data_obs_list = run(Xinit[0], tau, niter, AR, AC, lR, lC)[1].unsqueeze(0)

## batchsize number of initial positions
for i in range(1, batchsize):
    data_obs_list = torch.cat((data_obs_list, run(Xinit[i], tau, niter, AR, AC, lR, lC)[1].unsqueeze(0)),
                              dim=0)


# print(MMD(data_obs_list, data_obs_list) + positive_penalizer(AR) + positive_penalizer(AC) + positive_penalizer(lR) + positive_penalizer(lC) + positive_penalizer(lC - lR) + positive_penalizer(AR-AC))

## ---------------------------- LEARNING ------------------------------

hist_final = np.zeros((5,epoch))
for k in range(ntest_al):
    ## Initiate from random parameters
    AR_p = torch.tensor(5*np.random.random(1), requires_grad=True, device=device)
    AC_p = torch.tensor(AR_p.item()*np.random.random(1), requires_grad=True, device=device)
    lC_p = torch.tensor(5*np.random.random(1), requires_grad=True, device=device)
    lR_p = torch.tensor(lC_p.item()*np.random.random(1), requires_grad=True, device=device)

    # AR_p = torch.tensor(3.9, requires_grad=True, device=device)
    # AC_p = torch.tensor(1.1, requires_grad=True, device=device)
    # lC_p = torch.tensor(2.1, requires_grad=True, device=device)
    # lR_p = torch.tensor(0.4, requires_grad=True, device=device)

    hist = np.zeros((5, epoch))
    ## pytorch optimizer
    optimizer = optim.Adam([AR_p, AC_p, lR_p, lC_p], lr = lr)
    for e in range(epoch):
        data_list = run(Xinit[0], tau, niter, AR, AC, lR, lC)[1].unsqueeze(0)
        ## batchsize number of initial positions
        for i in range(1, batchsize):
            data_list = torch.cat((data_list, run(Xinit[i], tau, niter, AR_p, AC_p, lR_p, lC_p)[1].unsqueeze(0)),
                                       dim=0)
        ## Compute MMD loss
        L_loc = MMD(data_obs_list, data_list) + positive_penalizer(AR_p) + positive_penalizer(AC_p) + positive_penalizer(lR_p) + positive_penalizer(lC_p) + positive_penalizer(lC_p - lR_p) + positive_penalizer(AR_p-AC_p)
        # print(MMD(data_obs_list, data_obs_list).item())
        # print(L_loc.item())
        # print(AR_p.item(), AR.item())
        # print(AC_p.item(), AC.item())
        # print(lR_p.item(), lR.item())
        # print(lC_p.item(), lC.item())
        # breakpoint()
        ## Compute the gradient
        optimizer.zero_grad()
        L_loc.backward()
        hist[0, e] = L_loc
        # print(L_loc.item())
        ## Optim step
        optimizer.step()


        ## Store results
        hist[1, e] = AR_p
        hist[2, e] = AC_p
        hist[3, e] = lR_p
        hist[4, e] = lC_p
    ## If best loss, store it
    if hist[0,-1] < loss:
        hist_final = hist.copy()
        loss = hist[0,-1]
        print("Better loss ", hist_final[:,-1])
    print("random iteration ", k, " over ", ntest_al, ", loss =", hist[0,-1],
          "parameters : ", AR_p.item(), AC_p.item(), lR_p.item(), lC_p.item())

plt.figure()
loss_hist = hist_final[0, :]
plt.title("Loss evolution")
XX = np.arange(0, epoch)
plt.plot(XX, loss_hist)
# plt.figure()
AR_hist = hist_final[1, :]
AC_hist = hist_final[2, :]
lR_hist = hist_final[3, :]
lC_hist = hist_final[4, :]
# plt.title("Parameters evolution")

XX = np.arange(0, epoch)

plt.figure()
plt.plot(XX, AR_hist, label='AR')
plt.plot(XX, AR.item()*np.ones_like(AR_hist), label = "True AR")
plt.legend()

plt.figure()
plt.plot(XX, AC_hist, label='AC')
plt.plot(XX, AC.item()*np.ones_like(AC_hist), label = "True AC")
plt.legend()

plt.figure()
plt.plot(XX, lR_hist, label='lR')
plt.plot(XX, lR.item()*np.ones_like(lR_hist), label = "True lR")
plt.legend()

plt.figure()
plt.plot(XX, lC_hist, label='lC')
plt.plot(XX, lC.item()*np.ones_like(lC_hist), label='True lC')
plt.legend()

print(" True parameters ", np.round([AR.item(), AC.item(), lR.item(), lC.item()], 3))
print(" Final parameters ", np.round([AR_p.item(), AC_p.item(), lR_p.item(), lC_p.item()], 3))


plt.show()