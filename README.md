# Crowd Motion Parameter Identification

Project realized by Félicien BOURDIN during his PhD Thesis, with the help of Gabriel Peyré (CNRS)
The work of Félicien BOURDIN is supported by the ERC Grant NORIA.

See chapter "Problème inverse : identification de paramètres" 
for a precise description of the models and the simulations. 

## Wasserstein vs L2 Loss

``` WassVsL2.py ```
runs a comparizon between the Wasserstein Loss and the L2 Loss 
to identify the velocity shifting a density that varies quickly in space.

## Learn a parametric field

```learn_U_param.py```
recovers the parameters of a velocity field of the form
<img src="https://latex.codecogs.com/gif.latex?U_{r,\omega}(t) = r \mathrm{sin}(\omega t)" />. An initial 1D density is transported by a velocity field of this form under congestion constraint. A gradient descent on the parameters identifies the actual parameters from the observation of the motion of the density.

## Microscopic learning

```micro_learning.py```
recovers the parameters of a Monge interaction potential from the observation of the motion of a collection of particles subjected to it.



## Macroscopic learning

```learn_macro.py```
learns a potential field from the observation of the motion of a macroscopic crowd.

Precomputations are available in the folder 
```precomputed_imgs```

