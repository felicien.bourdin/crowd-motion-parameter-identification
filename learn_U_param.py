import numpy as np
import matplotlib.pyplot as plt
import torch
import torch.optim as optim
from geomloss import SamplesLoss
import time
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
torch.autograd.set_detect_anomaly(True)
import os
import sys
sys.path.insert(0, sys.path[0] + "/scripts")
from scheme import splitting

## Gradient Matrix
def mat_grad_centered(N):
    return N/2*(torch.diag(torch.ones(N-1),+1) +torch.diag(torch.ones(1),-N+1) - torch.diag(torch.ones(N-1),-1) - torch.diag(torch.ones(1),N-1))

## Parametric field
def U_param(r,theta,t):
    result = r*torch.sin(theta*t)
    return result

## Gradient descent to learn the parameters
def learn_U_param(rho_init, Zobs, ngrad, params, r_guess=0., theta_guess =0.,max_iter=20, verbose = True ):

    rho_max = params["rho_max"]
    ttot = params['ttot']
    dt = params['dt']
    dx = params["dx"]
    Npts = int(1/dx)

    positions = torch.tensor(np.arange(0, 1 , 1/Npts) + 1 / 2 / Npts).reshape((Npts,1))
    lab = torch.tensor(np.arange(0,Npts))

    ## Put the variables on the device
    r = torch.tensor(float(r_guess)).to(device)
    r.requires_grad = True
    theta = torch.tensor(float(theta_guess)).to(device)
    theta.requires_grad = True

    ## Logs
    r_hist = [r.item()]
    theta_hist = [theta.item()]
    loss_hist=[]
    pert_hist=[]

    ## Pytorch optimizer
    optimizer = optim.LBFGS([r,theta], max_iter=max_iter, lr=1)

    global j
    j=0
    for i in range(ngrad):
        def closure():
            global j
            j+=1
            ## Generate data
            field = U_param(r,theta,positions)
            Z = splitting(rho_init, dt, field, ttot,rho_max,verbose = False)

            ## Compute loss
            Loss = SamplesLoss(loss="sinkhorn", p=2, blur=.05)
            Wass = Loss(lab, Z[:,-1], positions, lab, Zobs[:,-1], positions)
            for k in range(int(ttot/dt)):
                Loss_loc = SamplesLoss(loss="sinkhorn", p=2, blur=.05)
                a= Loss_loc(lab, Z[:,k], positions, lab, Zobs[:,k], positions)
                Wass= Wass + a
            Wass=Wass/int(ttot/dt)
            loss = Wass.item()


            if verbose:
                print("Iteration", i , " over " , ngrad, ", Grad step", j, ", Loss: ", loss,  ", r: ", r.item(), ", theta: ", theta.item())

            ## Fill the logs
            r_hist.append(r.item())
            theta_hist.append(theta.item())
            loss_hist.append(loss)

            ## Compute the gradients
            optimizer.zero_grad()
            Wass.backward()
            return Wass

        ## Gradient step
        optimizer.step(closure)

    return r_hist,theta_hist, loss_hist,pert_hist


if __name__=="__main__":
    ##---------------------- GENERATE SYNTHETIC DATA ----------------------

    ## Simulation parameters
    dx = 1/50
    dt = 0.02
    ttot = .5
    rho_max = 1.1
    params = {"dx":dx,"dt":dt,"ttot":ttot,"rho_max":rho_max}

    ## Initial density
    def frho(x):
        ## Periodic BC
        return np.sin(6 * x * np.pi) ** 2
    N = int(1/dx)
    X = torch.tensor(np.linspace(0,1-1/N,N)+1/2/N)
    rho_init = frho(X)

    ## Hidden parameters to recover
    r = torch.tensor([0.5])
    theta = torch.tensor([2])

    ## Generation of Zobs
    positions = torch.tensor(np.linspace(0, 1 - 1 / N, N) + 1 / 2 / N)
    field = U_param(r, theta, positions)
    Z_obs = splitting(rho_init,dt, field, ttot,rho_max,verbose=False)


    ##------------------------------- LEARNING -----------------------------------

    ## Learning parameters
    ngrad = 20
    max_iter = 1

    r_hist, theta_hist, loss_hist, pert_hist = learn_U_param(rho_init, Z_obs, ngrad, params,
                                                         r_guess=1, theta_guess=3, max_iter=max_iter)


    ## -------------------------------- PLOTS ------------------------------------

    ## Plot the loss history
    xx = np.arange(0, len(loss_hist))
    fig1, ax1 = plt.subplots(figsize=(10, 10))
    plt.plot(xx, loss_hist, label='loss')
    ax1.legend()
    plt.title("Loss as a function of the number of iterations")

    ## Plot the parameters history
    xx = np.arange(0, len(r_hist))
    fig2, ax2 = plt.subplots(figsize=(10, 10))
    plt.plot(xx, r_hist, label='r')
    plt.plot(xx, theta_hist, label='$\omega$')
    plt.xticks(np.arange(0, 20, step=2))
    ax2.legend()
    plt.show()

    ## Plot the space parameters
    etude_phase_space = True

    if etude_phase_space:
        resol = 50
        loss = np.zeros((resol, resol))
        i = 0
        for r in np.linspace(0, 4, resol):
            j = 0
            for theta in np.linspace(0, 4, resol):
                ## Generate data
                field = U_param(r, theta, positions)
                lab = torch.tensor(np.arange(0, N))
                Loss = SamplesLoss(loss="sinkhorn", p=2, blur=.05)
                Z = splitting(rho_init, dt, field, ttot, rho_max, verbose=False)

                ## Compute the loss
                positions = torch.tensor(np.arange(0, 1, 1 / N) + 1 / 2 / N).reshape((N, 1))
                Wass = Loss(lab, Z[:, -1], positions, lab, Z_obs[:, -1], positions)
                for k in range(int(ttot / dt)):
                    Loss_loc = SamplesLoss(loss="sinkhorn", p=2, blur=.05)
                    a = Loss_loc(lab, Z[:, k], positions, lab, Z_obs[:, k], positions)

                    Wass = Wass + a
                Wass = Wass / int(ttot / dt)
                loss[i, j] = Wass.item()
                print('r: ', r, '$\omega$: ', theta, "loss + pert: ", Wass.item())
                j += 1
            i += 1

        ## Plot 3D surface
        fig3, ax3 = plt.subplots(subplot_kw={"projection": "3d"})
        X = np.arange(0, 4, 4 / resol)
        Y = np.arange(0, 4, 4 / resol)
        X, Y = np.meshgrid(X, Y)
        surf = ax3.plot_surface(X, Y, loss.T, rstride=1, cstride=1,
                                cmap='twilight_shifted', edgecolor='none')
        ax3.set_xlabel('r')
        ax3.set_ylabel('$\omega$')
        ax3.set_zlabel('Loss')
        plt.title("Space of parameters")
        ax3.scatter(r_hist[1:], theta_hist[1:], loss_hist, color='r')
    plt.show()