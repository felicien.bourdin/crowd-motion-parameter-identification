import torch
import matplotlib.pyplot as plt
import time

def prox(p, sigma, D,rho_max):
    return torch.minimum(torch.ones_like(p)*rho_max,p*torch.exp(-sigma*D))

def crowd_motion_JKO_step(C,D,q, gamma,dt,rho_max, eps = 10e-10, nitmax = 30):
    # print("a",C.size(), D.size(), q.size(), gamma, dt, rho_max)
    ## C : cost matrix
    ## D : objective function
    ## gamma : reg parameter
    ## q : density at previous timestep

    n = D.shape[0]
    # print(D)
    a = torch.ones(n)
    b = torch.ones(n)
    u = torch.ones(n)
    v = torch.ones(n)
    w = torch.ones(n)
    x = torch.ones(n)

    xi = torch.exp(-C/gamma)

    l=0
    while True:
        # print(l%2)
        if l%2 == 0:
            at = a*w
            bt = q/((xi.T).matmul(at))
            # if torch.isnan(bt.any()):
            #     print(xi.matmul(at),'bt even')
            #     breakpoint()
            ut = w*a/at
            # if torch.isnan(ut.sum()):
            #     print( w, a, at,'ut even')
            #     breakpoint()
            vt = x*b/bt
            # if torch.isnan(vt.sum()):
            #     print(x,b,bt,'vt even')
            #     breakpoint()
        if l%2 == 1:
            bt = b*x
            # if torch.isnan(bt.sum()):
            #     print('bt odd', b ,x)
            #     breakpoint()
            at = prox(a*w*(xi.matmul(bt)), dt/gamma,D,rho_max)/(xi.matmul(bt))
            # if torch.isnan(at.sum()):
            #     print('at odd', a,w,xi,bt)
            #     breakpoint()
            ut = w*a/at
            vt = x*b/bt

        x = v
        w = u
        u = ut
        v = vt
        a = at
        b = bt

        # print(b.shape, xi.shape, a.shape, q.shape)
        # print("cout",torch.linalg.norm(b*(xi.matmul(a))-q), " iteration ",l)
        # print("l",l)
        # print("b",b)
        # print("xi", xi.matmul(a))
        # print("a",a)
        # print("w",w)
        # breakpoint()
        l+=1

        if torch.linalg.norm(b*(xi.matmul(a))-q) < eps:
            # print(rho_max,(a*w*(xi.matmul(bt))).shape)
            return prox(a*w*(xi.matmul(bt)), dt/gamma,D,rho_max)
        if torch.isnan(torch.linalg.norm(b*(xi.matmul(a))-q)):
            # print(b,xi,a,q)
            raise ValueError('NaN cost')
        if l>nitmax:
            # print(rho_max,(a*w*(xi.matmul(bt))).shape)
            return prox(a*w*(xi.matmul(bt)), dt/gamma,D,rho_max)

def crowd_motion_JKO(dt,nit,rho_i,f,rho_max,gamma=.01,boundary = 1, verbose = True):
    N=rho_i.shape[0]
    rho_hist = torch.zeros(N,N,nit+1)
    rho_hist[:,:,0] = rho_i[:,:]
    xx = (torch.arange(-boundary,N+boundary,1)+1/2)/N
    XX,YY = torch.meshgrid(xx,xx)
    XX = XX.reshape(((N+2*boundary)*(N+2*boundary),1))
    YY = YY.reshape(((N+2*boundary)*(N+2*boundary),1))
    pos = torch.cat([XX,YY], dim = 1)
    cost_mat = torch.sum((pos[:, None, :] - pos[None, :, :]) ** 2, dim=2)
    D_ext = f(pos).reshape((N + 2 * boundary)*(N + 2 * boundary))
    q = rho_i
    q_ext = torch.ones((N + 2 * boundary, N + 2 * boundary))
    if boundary >0:
        q_ext[boundary:-boundary, boundary:-boundary] = q
    else:
        q_ext = q
    q_ext = q_ext.reshape((2*boundary+N)*( 2*boundary+N))

    q_ext[torch.where(q_ext==0)] = 10e-7


    for k in range(nit):
        tps = time.time()
        # print(q.shape,D.shape, cost_mat.shape)
        # print(cost_mat.size(), D_ext.size(), q_ext.size())
        q_ext=crowd_motion_JKO_step(cost_mat,D_ext,q_ext,gamma,dt,rho_max)
        if boundary>0:
            rho_hist[:,:,k+1] = q_ext.reshape((N+2*boundary,N+2*boundary))[boundary:-boundary,boundary:-boundary]
        else:
            rho_hist[:, :, k + 1] = q_ext.reshape((N + 2 * boundary, N + 2 * boundary))
        q_ext = q_ext.reshape((N+2*boundary,N+2*boundary))
        if boundary>0:
            q_ext[:boundary,:] = 10e-7
            q_ext[-boundary:,:] = 10e-7
            q_ext[:,boundary] = 10e-7
            q_ext[:,-boundary:] = 10e-7
        q_ext = q_ext.reshape((N+2*boundary)*(N+2*boundary))
        if verbose:
            print("Iteration ",k," over ", nit, ", time: ", time.time()-tps, "mass: ", q_ext.sum().item())
    return rho_hist

if __name__=='__main__':
    def f(x):
        return 1/10*(torch.sqrt((x[:,0]-1.1)**2 + (x[:,1]-1/2)**2)*10)

    N=50
    dt=.01
    nit=20
    rho_max = 2
    # rho = torch.outer(torch.arange(0,N,1),torch.arange(0,N,1))+1
    rho = torch.ones((N,N))*rho_max
    rho_hist = crowd_motion_JKO(dt,nit,rho,f,rho_max,gamma=.0005)

    # plt.subplot(241)
    # plt.imshow(rho_hist[:,:,0],vmin=0,vmax=1)
    # plt.colorbar()
    # plt.title('time: '+ str(0))
    # plt.subplot(242)
    # plt.imshow(rho_hist[:,:,1],vmin=0,vmax=1)
    # plt.colorbar()
    # plt.show()
    # breakpoint()
    plt.figure(figsize=(10,10))
    xx = (torch.arange(0, N , 1) + 1 / 2) / N
    XX, YY = torch.meshgrid(xx, xx)
    XX = XX.reshape(((N) * (N), 1))
    YY = YY.reshape(((N) * (N), 1))
    pos = torch.cat([XX,YY], dim = 1)
    D = f(pos).reshape((N,N))
    plt.imshow(D)
    plt.colorbar()
    plt.figure(figsize=(30,20))
    plt.subplot(241)
    plt.imshow(rho_hist[:,:,0],vmin=0,vmax=1)
    plt.colorbar()
    plt.title('time: '+ str(0))
    plt.subplot(242)
    plt.imshow(rho_hist[:,:,int(nit/7)],vmin=0,vmax=1)
    plt.colorbar()
    plt.title('time: '+ str(int(nit/7)*dt))
    plt.subplot(243)
    plt.imshow(rho_hist[:,:,int(2*nit/7)],vmin=0,vmax=1)
    plt.colorbar()
    plt.title('time: '+ str(int(2*nit/7)*dt))
    plt.subplot(244)
    plt.imshow(rho_hist[:,:,int(3*nit/7)],vmin=0,vmax=1)
    plt.colorbar()
    plt.title('time: '+ str(int(3*nit/7)*dt))
    plt.subplot(245)
    plt.imshow(rho_hist[:,:,int(4*nit/7)],vmin=0,vmax=1)
    plt.colorbar()
    plt.title('time: '+ str(int(4*nit/7)*dt))
    plt.subplot(246)
    plt.imshow(rho_hist[:,:,int(5*nit/7)],vmin=0,vmax=1)
    plt.colorbar()
    plt.title('time: '+ str(int(5*nit/7)*dt))
    plt.subplot(247)
    plt.imshow(rho_hist[:,:,int(6*nit/7)],vmin=0,vmax=1)
    plt.colorbar()
    plt.title('time: '+ str(int(6*nit/7)*dt))
    plt.subplot(248)
    plt.imshow(rho_hist[:,:,int(7*nit/7)],vmin=0,vmax=1)
    plt.title('time: '+ str(int(7*nit/7)*dt))
    plt.colorbar()

    plt.show()