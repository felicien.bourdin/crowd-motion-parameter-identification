import torch
from JKO_scheme_with_exit import crowd_motion_JKO
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from geomloss import SamplesLoss
torch.autograd.set_detect_anomaly(True)
import time
import matplotlib.pyplot as plt
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
import numpy as np
import os
import copy

class Net(nn.Module):
    ## Reseau de neurone pour approximer le champ de potentiel D
    def __init__(self):
        super(Net, self).__init__()
        self.fc1 = nn.Linear(2,20)
        self.fc2 = nn.Linear(20,1, bias = False)

    def forward(self, x):
        xx = F.relu(self.fc1(x),inplace=False)
        y = self.fc2(xx)
        # print(xx,y)
        return y

def L2_loss(Z,Zobs):
    return torch.linalg.norm(Z-Zobs)

def learn_f(rho_init, Zobs, ngrad, net, params,lr, rho_max_guess = 1., gamma=.01, max_iter=20,loss="balanced", verbose=True, boundary = 0, learn_rho_max = False):
        dt = params["dt"]
        ttot = params["ttot"]
        dx = params["dx"]
        nit = int(ttot/dt)
        Npts = int(1 / dx)

        if loss == "unbalanced":
            reach = .2
        else:
            reach = None

        ## Densité maximale à estimer
        rho_max = torch.tensor([rho_max_guess])
        rho_max.requires_grad = True
        rho_max.to(device)

        ## Labels pour SamplesLoss
        lab = torch.arange(0, Npts*Npts)

        ## Positions pour SamplesLoss
        xx = (torch.arange(0, N, 1) + 1 / 2) / N
        XX, YY = torch.meshgrid(xx, xx)
        XX = XX.reshape((N * N, 1))
        YY = YY.reshape((N * N, 1))
        positions = torch.cat([XX, YY], dim=1)

        ## Création historiques
        loss_hist = []
        net_hist = []
        rho_max_hist = [rho_max.item()]

        ## Paramètres d'opti : champ D et rho_max
        var = list(net.parameters())
        if learn_rho_max:
            var= var+[rho_max]
        optimizer = optim.LBFGS(var,lr, max_iter=max_iter)


        ## Compteur de pas de gradient
        global j
        j=0

        for i in range(ngrad):
            def closure(): ## Pour LBFGS
                global j
                j += 1
                tps = time.time()
                ## Calcul film
                Z = crowd_motion_JKO(dt,nit,rho_init,net,rho_max,gamma=gamma, verbose = False, boundary=boundary)

                ## Calcul du Loss

                Loss = SamplesLoss(loss="sinkhorn", p=2, reach=reach, blur=.05)
                Wass = Loss(lab, Z[:,:, -1].reshape(N*N), positions, lab, Zobs[:,:, -1].reshape(N*N), positions)

                for k in range(int(ttot / dt)):
                    a = Loss(lab, Z[:,:, k].reshape(N*N), positions, lab, Zobs[:,:, k].reshape(N*N), positions)
                    Wass = Wass + a
                Wass /= (int(ttot / dt)+1)
                losse = Wass.item()

                if verbose:
                    print("Iteration ", i, " over ", ngrad, ", Grad Step ", j,"  Loss: ", losse, "rho_max: ",rho_max.item()," Time: ", time.time() - tps)
                loss_hist.append(losse)

                ## Calcul du gradient
                optimizer.zero_grad()
                Wass.backward()

                return Wass

            ## Pas du gradient
            optimizer.step(closure)
            net_hist.append(copy.copy(net))
            rho_max_hist.append(rho_max.item())
        return net, rho_max, loss_hist, net_hist, rho_max_hist

def adaptative_learning_f(rho_init, Zobs, ngrad, net, params,lr, decay,epochs, rho_max_guess = 1.,gamma=.01,max_iter=20, loss="balanced", verbose=True, boundary = 0, learn_rho_max = False):
    ## Learning à taux variable décroissant exponentiellement
    net_loc = net
    loss_histt = []
    net_histt = []
    rho_max_histt=[rho_max_guess]
    for k in range(epochs):
        ## Une epoch par learning rate
        tps_loc = time.time()
        lrk = lr*decay**(-k)
        print("---------------- EPOCH ", k+1, " over ", epochs,' lr =',lrk,'----------------')
        net_loc,rho_max, loss_hist, net_hist,rho_max_hist = learn_f(rho_init, Zobs, ngrad, net_loc, params, lrk,rho_max_guess=rho_max_histt[-1], gamma=gamma,max_iter =max_iter,loss=loss, verbose=verbose,boundary=boundary, learn_rho_max=learn_rho_max)
        loss_histt = loss_histt+loss_hist
        net_histt = net_histt+net_hist
        rho_max_histt = rho_max_histt+rho_max_hist
        print("---------------- Time of EPOCH ", k,": ", time.time()-round(tps_loc,3),", loss = ", loss_hist[-1],'----------------')
        index = loss_histt.index(min(loss_histt))
        print(len(loss_histt), len(net_histt))
        net_loc = net_histt[index]


    return net_loc, loss_histt, net_histt, rho_max_histt

def graphique(rho_hist, title = '', vmax=None):
    fig = plt.figure(figsize=(30,20))
    plt.suptitle(title)
    plt.subplot(241)
    plt.imshow(rho_hist[:,:,0], vmin=0, vmax = vmax, origin='lower', extent=[0,1,0,1])
    # plt.colorbar()
    plt.title('time: '+ str(0))
    plt.subplot(242)
    plt.imshow(rho_hist[:,:,int(nit/7)], vmin=0, vmax = vmax, origin='lower', extent=[0,1,0,1])
    # plt.colorbar()
    plt.title('time: '+ str(int(nit/7)*dt))
    plt.subplot(243)
    plt.imshow(rho_hist[:,:,int(2*nit/7)], vmin=0, vmax = vmax, origin='lower', extent=[0,1,0,1])
    # plt.colorbar()
    plt.title('time: '+ str(int(2*nit/7)*dt))
    plt.subplot(244)
    plt.imshow(rho_hist[:,:,int(3*nit/7)], vmin=0, vmax = vmax, origin='lower', extent=[0,1,0,1])
    # plt.colorbar()
    plt.title('time: '+ str(int(3*nit/7)*dt))
    plt.subplot(245)
    plt.imshow(rho_hist[:,:,int(4*nit/7)], vmin=0, vmax = vmax, origin='lower', extent=[0,1,0,1])
    # plt.colorbar()
    plt.title('time: '+ str(int(4*nit/7)*dt))
    plt.subplot(246)
    plt.imshow(rho_hist[:,:,int(5*nit/7)], vmin=0, vmax = vmax, origin='lower', extent=[0,1,0,1])
    # plt.colorbar()
    plt.title('time: '+ str(int(5*nit/7)*dt))
    plt.subplot(247)
    plt.imshow(rho_hist[:,:,int(6*nit/7)], vmin=0, vmax = vmax, origin='lower', extent=[0,1,0,1])
    # plt.colorbar()
    plt.title('time: '+ str(int(6*nit/7)*dt))
    plt.subplot(248)
    im=plt.imshow(rho_hist[:,:,int(7*nit/7)-1], vmin=0, vmax = vmax, origin='lower', extent=[0,1,0,1])
    plt.title('time: '+ str(int(7*nit/7)*dt))
    # plt.colorbar()

    fig.subplots_adjust(right=0.9)
    cbar_ax = fig.add_axes([0.95, 0.15, 0.03, 0.7])
    fig.colorbar(im, cax = cbar_ax)

    return fig

if __name__=='__main__':
    ## Parameters

    key = 'real_data'

    ## ----------------------------------------- DATA AND PARAMETERS ----------------------------------------

    if key == 'data_synth':
        # Simulation parameters
        N = 50
        dx = 1 / N
        dt = .01
        nit = 10
        ttot = nit * dt
        boundary = 0 ## No way out
        learn_rho_max = False

        ## Generation data obs

        rho_max = 1.2
        rho = torch.outer(torch.arange(0,N),torch.arange(0,N))/N/N

        params = {}
        params["dt"] = dt
        params["ttot"] = ttot
        params["dx"] = dx
        params["nit"] = nit
        print("Parameters: ", params)

        ## Learning parameters

        gamma = .01
        ngrad = 20
        lr = .8
        decay = 2
        epochs = 2
        max_iter = 20

        def f(x):
            return torch.sqrt((x[:, 0] - 1 / 2) ** 2 + (x[:, 1] - 1 / 2) ** 2) * 10
        print('Generating Zobs ...')
        Zobs = crowd_motion_JKO(dt,nit,rho,f,1.,gamma=gamma,verbose = False,boundary=0)
        print('... Zobs Generated!')

    if key == "real_data":
        ## On récupère les données, sous format N*N*nit
        Zobs = torch.tensor(np.load('040_c_56_h-.npy'),dtype=torch.float32)[:,:,::40]
        nit = Zobs.shape[2]
        N = Zobs.shape[1]
        dx = 1/N
        rho = Zobs[:,:,0] # densité initiale
        dt = 1/25 # vient des metadata de la vidéo
        ttot = nit*dt

        rho_max =Zobs.max()
        print(rho_max)
        params = {}
        params["dt"] = dt
        params["ttot"] = ttot
        params["dx"] = dx
        params["nit"] = nit
        print("Parameters: ", params)

        ## Learning parameters

        gamma = .01
        ngrad = 10
        lr = .01
        decay = 2
        epochs = 3
        max_iter = 30
        learn_rho_max = True
        boundary = 1

    ## ----------------------------------------------- LEARNING -------------------------------------------

    ## On part d'un champ quelconque

    net = Net()

    ## apprentissage

    net_loc, loss_histt, net_histt, rho_max_histt = adaptative_learning_f(rho, Zobs, ngrad, net, params, lr, decay, epochs, rho_max_guess=rho_max, gamma=.01,
                          max_iter=max_iter, loss="balanced", verbose=True, learn_rho_max = learn_rho_max, boundary=boundary)

    # net, rho_max, loss_hist, net_hist, rho_max_hist= learn_f(rho, Zobs, ngrad, net, params,lr, rho_max_guess =rho_max, gamma=gamma, max_iter=max_iter,loss="balanced", verbose=True)

    ## ---------------------------------------------- GRAPHIQUE ----------------------------------------------

    if key == "real_data":
        vmax = None
    else:
        vmax = 1

    Z = crowd_motion_JKO(dt, nit, rho, net,rho_max_histt[-1], gamma=gamma, verbose=False,boundary=0).detach().numpy().reshape((N,N,nit+1))

    t03 = 0
    t13 =int(nit/3)
    t23 =int(2*nit/3)
    t33 =int(3*nit/3-1)

    fig1 = graphique(Zobs, "Zobs", vmax = vmax)
    fig2 = graphique(Z, "Z", vmax = vmax)

    xx = (torch.arange(0, N, 1) + 1 / 2) / N
    XX, YY = torch.meshgrid(xx, xx)
    XX = XX.reshape((N * N, 1))
    YY = YY.reshape((N * N, 1))
    positions = torch.cat([XX, YY], dim=1)
    pot = net(positions).reshape((N, N)).detach().numpy()

    fig3 , ax3= plt.subplots(figsize=(10,10))
    ax3.set_xlim(0,1)
    ax3.set_ylim(0,1)
    ax3.imshow(pot)

    fig4 , ax4= plt.subplots(figsize=(10,10))
    xx = np.arange(len(loss_histt))
    ax4.plot(xx, loss_histt)
    ax4.set_yscale('log')


    i = 0
    folder_name = 'runs/run' + str(i)
    while os.path.exists(folder_name):
        i += 1
        folder_name = 'runs/run' + str(i)

    os.makedirs(folder_name)


    ## Learning parameters


    fichier = open(folder_name + "/log.txt", "w+")
    fichier.write("----------- Paramètres simu :----------" +"\n")
    fichier.write("dx : " + str(dx) + "\n")
    fichier.write("dt : " + str(dt) + "\n")
    fichier.write("ttot : " + str(ttot) + "\n")
    fichier.write("nit : "+str(nit)+ "\n")

    fichier.write("----------- Paramètres learning :----------" + "\n")
    fichier.write("gamma : " + str(gamma) + "\n")
    fichier.write("ngrad : " + str(ngrad) + "\n")
    fichier.write("lr : " + str(lr) + "\n")
    fichier.write("decay : " + str(decay) + "\n")
    fichier.write("epochs : " + str(epochs) + "\n")
    fichier.write("max_iter : " + str(max_iter) + "\n")

    for i in range(len(loss_histt)):
        fichier.write(
            "Grad step " + str(i) + " over " + str(len(loss_histt)) + " : Loss = " + str(loss_histt[i]) + "\n")
    fichier.close()

    fig1.savefig(folder_name+"/Zobs.png")
    fig2.savefig(folder_name+"/Z.png")
    fig4.savefig(folder_name+"/loss.png")

    if key == "real_data":
        fig3.savefig(folder_name + "/potentiel.png")

    if key == "data_synth":
        fig5 = plt.figure(figsize = (20,10))

        pot1 = f(positions).reshape((N,N))
        pot2 = net(positions).reshape((N,N)).detach().numpy()

        plt.subplot(121)
        xx = (torch.arange(0, N, 1) + 1 / 2) / N
        XX, YY = torch.meshgrid(xx, xx)
        XX = XX.reshape((N * N, 1))
        YY = YY.reshape((N * N, 1))
        positions = torch.cat([XX, YY], dim=1)

        vmax = max(pot1.max(),pot2.max())

        plt.imshow(pot1, origin='lower', vmin = 0, vmax = vmax, extent=[0,1,0,1])
        # plt.colorbar()
        plt.title('Exact potential')
        plt.subplot(122)
        im=plt.imshow(pot2, vmin = 0, vmax = vmax, origin='lower', extent=[0,1,0,1])
        plt.title('Estimated potential')
        # plt.colorbar()
        fig5.subplots_adjust(right=0.9)
        # cbar_ax = fig.add_axes([0.85, 0.15, 0.05, 0.7])
        cbar_ax = fig5.add_axes([0.95, 0.15, 0.03, 0.7])
        fig5.colorbar(im, cax = cbar_ax)
        fig5.savefig(folder_name+"/potentials.png")
