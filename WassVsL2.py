import numpy as np
import matplotlib.pyplot as plt
import torch
torch.autograd.set_detect_anomaly(True)
import ot

## Distance Matrix
def distmat_square_keops(X, Y):
    return torch.minimum((X[:,None] - Y[None,:])**2,1-torch.abs((X[:,None] - Y[None,:]))**2) ## 1-periodicité

## Wasserstein loss between two densities defined on a line
def LWauto(rho1,rho2,xmin,xmax):
    N = rho1.shape[0]
    X = torch.arange(xmin,xmax, 1/N)
    mat = distmat_square_keops(X,X)
    W=ot.sinkhorn2(rho1/N+10e-6,rho2/N+10e-6,mat,0.1)
    return W

## L2 loss
def L2auto(rho1,rho2):
    N=rho1.shape[0]
    return torch.sum((rho1-rho2)**2).item()/N

## Translation of rho by vector u between t
def transl(rho,u,t,dx):
    tab = torch.zeros_like(rho)
    N = tab.shape[0]
    shift =int(np.floor(t*u/dx))

    if shift>=0:
        tab[:shift] = rho[N-shift:]
        tab[shift:] = rho[:N-shift]
    else:
        tab[:N+shift] = rho[- shift:]
        tab[N+shift:] = rho[:-shift]
    return tab

# --------------- PARAMETERS ----------------

## Meshsize
N=1000

## Initial density: spikes
rhoi = torch.zeros(N)
for x in range(1,5):
    for k in range(N):
        rhoi[k] += np.exp(-(k/N-x/10)**2/0.02**2)

## Show initial density
plt.plot(np.arange(0,1,1/N),rhoi)

## Simulation parameters
ttot = .5 # Total Time
dt = .05 # timestep
nit = int(ttot/dt) # Iteration of the scheme
neps = 100 # Number of points for epsilon

L2 = np.zeros(neps)
LW = np.zeros(neps)
u=.5 # True velocity

compt = 0
epss = np.arange(-.5,.5,1/(neps)) # Parameter space

## ----------------- COMPARISON L2 vs WASSERSTEIN ------------

## Compute the losses
for eps in epss:
    print(eps)
    for k in range(nit):
        t = dt*k
        rho1 = transl(rhoi,u,t,1/N)
        rho2 = transl(rhoi,u+eps,t,1/N)
        LW[compt]+=LWauto(rho1,rho2,0,1)
        L2[compt]+=L2auto(rho1,rho2)
    compt+=1
LW/=nit
L2/=nit

fig,ax = plt.subplots()
plt.plot(epss,LW,label='Wasserstein')
plt.xlabel("epsilon")
plt.ylabel("Loss")
plt.legend()
fig2,ax2 = plt.subplots()
plt.plot(epss,L2,label='L2')
plt.xlabel("epsilon")
plt.ylabel("Loss")
plt.legend()
plt.show()