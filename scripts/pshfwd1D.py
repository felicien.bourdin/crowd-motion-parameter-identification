import numpy as np
import matplotlib.pyplot as plt
import torch
import time

def pshfwd(rho,tau,field):
    ## Push forward rho_t = (1+tauU)#rho avec CL périodiques
    N=rho.size()[0]
    pos = torch.tensor(np.linspace(0,1-1/N,N)+1/2/N)
    # print(pos)
    ## On calcule où chaque cellule va se faire transporter
    # print(pos.size(),tau,field.size())
    pos =pos +  tau * field.reshape((N))
    # print(tau*field,pos,tau*field+pos)
    # print(pos[12])
    ## En fonction de la position du centre on dispatche de la masse entre la cellule et celle qui la suit
    pos2 = torch.floor(N*pos-1/2).long()
    # print(pos2)
    coef = N*pos-1/2-pos2
    ## Masse sur la cellule
    rho_cell = (1-coef)*rho
    ## Masse sur la cellule de droite
    rho_cell_d = coef*rho
    # print(rho_cell.sum()+rho_cell_g.sum())
    tab = torch.tensor(np.zeros(N))

    # tps=time.time()


    ## WAY FASTER BUT BACKPROP NOT AVAILABLE FOR BINCOUNT ....

    # tab2 = torch.tensor(np.zeros(N))
    # tab2 += torch.bincount(pos2%N,weights = rho_cell)
    # tab2 += torch.bincount((pos2+1)%N,weights = rho_cell_d)

    for k in range(N):
        tab[pos2[k]%N]+=rho_cell[k]
        tab[(pos2[k]+1)%N]+=rho_cell_d[k]

    # print(tab,tab2)
    # breakpoint()
    # print(time.time()-tps)
    # print(rho,'\n',pos,'\n',coef,'\n',rho_cell,'\n',rho_cell_d,'\n',tab)
    # breakpoint()
    return tab

if __name__=='__main__':

    def frho(x):
        ## CL périodiques
        return np.sin(6 * x * np.pi)**2


    def U(x):
        ## CL périodiques
        return x * (1 - x)

    N = 200
    tau = 0.05
    X = torch.tensor(np.linspace(0,1,N))
    rho = frho(X)

    xx = np.linspace(0,1,N)
    plt.plot(xx,rho,color='red')
    field = U(xx)
    rho_t = pshfwd(rho,tau,field)

    print(rho.sum(),rho_t.max())

    plt.plot(xx,rho_t,color='blue')
    plt.show()