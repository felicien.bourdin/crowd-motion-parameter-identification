import numpy as np
import matplotlib.pyplot as plt
import torch
from pshfwd1D import pshfwd

def frho(x):
    return np.abs(np.sin(6 * x * np.pi))


def mat_lap_cl(N):
    ## Matrice du laplacien 1D avec une CL en N+1eme ligne : moyenne de la pression = 0
    mat_lap = -N**2*(2*torch.eye(N)-torch.diag(torch.ones(N-1),-1)-torch.diag(torch.ones(N-1),1)-np.diag(torch.ones(1),1-N)-np.diag(torch.ones(1),N-1))
    mat_lap_cl = torch.ones((N+1,N))
    mat_lap_cl[:N,:N] = mat_lap
    return mat_lap_cl

def mat_grad(N):
    return N*(torch.eye(N)- torch.diag(torch.ones(N-1),-1) - torch.diag(torch.ones(1),N-1))


def mat_grad_centered(N):
    return N/2*(torch.diag(torch.ones(N-1),+1) +torch.diag(torch.ones(1),-N+1) - torch.diag(torch.ones(N-1),-1) - torch.diag(torch.ones(1),N-1))

def proj_lap(rho,rho_max,key="centered"):
    N = rho.size()[0]
    ## On récupère la masse qui dépasse de 1
    alpha = rho-rho_max
    alpha[alpha<0] = 0
    ## On ajoute la CL
    alpha = torch.cat((alpha,torch.tensor(np.array([0])))).type(torch.FloatTensor)
    lap = mat_lap_cl(N)
    ## -Lap(p) = alpha
    # print(lap.size(),alpha.size())
    # print(alpha)
    p = torch.linalg.inv((-lap).T.matmul(-lap)).matmul(-lap.T).matmul(alpha)
    # print(lap.dot(p[:,0]).size(),alpha.size())
    # breakpoint()
    # print(p.size())
    ## u = -Grad p
    # print(mat_grad(10))
    # breakpoint()
    if key == "biaised":
        u = -mat_grad(N).matmul(p)
    if key == "centered":
        u = -mat_grad_centered(N).matmul(p)
    ## Plan de transport de rho à sa proj de la forme 1+eps u
    rho_t = pshfwd(rho,1,u)
    # print(-lap.dot(p),alpha)
    # return(-lap.dot(p),alpha)
    # print(alpha)
    # x = np.linspace(0,1,N+1)
    # plt.plot(x,-lap.dot(p))
    # plt.plot(x,alpha)
    # plt.show()
    return(rho_t)



if __name__=="__main__":
    N = 10

    mat = -mat_lap_cl(N)

    # print(np.linalg.matrix_rank(mat_lap_cl(N)))
    # breakpoint()
    tau = 0.01
    X = torch.tensor(np.linspace(0,1,N))
    rho = (1+tau)*frho(X)
    rho_t = proj_lap(rho,1)
    print(rho.sum(),rho_t.sum())
    print(rho.max(),rho_t.max())


    plt.plot(X,rho)
    plt.plot(X,rho_t, color='red')
    plt.show()