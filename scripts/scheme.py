import numpy as np
import matplotlib.pyplot as plt
import torch
import matplotlib.animation as animation
from pshfwd1D import pshfwd
from proj_lapl import proj_lap
import time
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

def splitting(rho, tau, field, ttot,rho_max,verbose = True):
    Nit = int(ttot/tau)
    rho_hist = torch.tensor(np.zeros((rho.size()[0],Nit+1)))
    rho_temp = rho
    rho_temp.to(device)
    rho_hist[:,0] = rho_temp
    for k in range(Nit):
        if verbose:
            print("Splitting step " + str(k) + " over " + str(Nit))
            print("     push forward ...")
            tps = time.time()
        rho_temp = pshfwd(rho_temp,tau,field)
        if verbose:
            print("     ... done ! Time: "+str(time.time()-tps))
            print("     projection ...")
            tps = time.time()
        rho_temp = proj_lap(rho_temp,rho_max)
        if verbose:
            print("     ... done ! Time: "+str(time.time()-tps))
            print("Sum: ", rho_temp.sum().item())
        rho_hist[:,k+1] = rho_temp
    return rho_hist

if __name__=="__main__":
    def frho(x):
        ## CL périodiques
        return np.sin(6 * x * np.pi)**2

    def U(x):
        ## CL périodiques
        return x * (1 - x)+0.1

    N = 200
    tau = 0.02
    ttot = 1
    rho_max = 1.1
    X = torch.tensor(np.linspace(0,1-1/N,N)+1/2/N)
    # print(X)
    # breakpoint()
    rho = frho(X)
    field = U(X)
    rho_hist = splitting(rho,tau, field, ttot,rho_max,verbose=True)

    ## Etude diffusivité du pas fwd

    diff_etude=False

    if diff_etude:

        rangeN = np.arange(10,250,10)
        print(rangeN)
        rangetau = np.arange(0.01,0.2,0.005)
        print(rangetau)
        nbN = len(rangeN)
        nbTau = len(rangetau)
        diff = np.zeros((nbN,nbTau))
        # plt.figure(figsize=(30,20))
        i,j = 0,0
        threshold = .99
        for N in rangeN:
            for tau in rangetau:
                print(N,tau)
                X = torch.tensor(np.linspace(0, 1, int(N)))
                rho = frho(X)
                field = U(X)
                rho_hist = splitting(rho,tau, field, ttot, rho_max,verbose=False)

                # if np.max(rho_hist[:,-1].numpy())> threshold:
                diff[i,j] = np.max(rho_hist[:,-1].numpy())
                j+=1
            i+=1
            j=0
        plt.imshow(diff,origin='lower',extent = [0.01,0.2,10,250], aspect = 1/1000)
        # plt.xlim(10, 250)
        # plt.ylim(0.01,0.1)
        plt.show()
                # maxrho = np.amax(rho_hist.numpy(),axis=0)
                # print(maxrho.shape)
                # xx = np.linspace(0,ttot,int(ttot/tau)+1)
                # plt.subplot(121)
                # plt.plot(xx,maxrho,label='N = '+str(N))
                # plt.title('The maximum of rho(t) as a function of time as the mesh size varies')
                # plt.legend()
                # plt.subplot(122)
                # plt.plot(X,rho_hist[:,-1],label='N = '+str(N))
                # plt.title('The final position rho(ttot).')
                # plt.legend()

    anim = True
    if anim:

        fig,ax = plt.subplots()  # initialise la figure
        line, = plt.plot([], [])
        plt.xlim(0, 1)
        plt.ylim(-0.2, 1.2)

        Nquiv = 10
        Xquiv = np.linspace(0,1,Nquiv)
        Yquiv = torch.tensor(.5*np.ones(Nquiv))
        Uquiv = U(Xquiv)
        Vquiv = 0.*Uquiv
        plt.quiver(Xquiv,Yquiv,Uquiv,Vquiv, width = 0.005)

        # fonction à définir quand blit=True
        # crée l'arrière de l'animation qui sera présent sur chaque image
        def init():
            line.set_data([], [])
            return line,


        title = ax.text(0.5, 1.1, "", bbox={'facecolor': 'w', 'alpha': 0.5, 'pad': 5},
                        transform=ax.transAxes, ha="center")

        def animate(i):
            Y = rho_hist[:,i]
            line.set_data(X, Y)
            title.set_text('Time: '+str(np.round(i*tau,2)))
            return line,


        ani = animation.FuncAnimation(fig, animate, init_func=init, frames=int(ttot/tau), interval=100, repeat=True)
        ani.save('temp_videos/Scheme1D.mp4')
        plt.show()
